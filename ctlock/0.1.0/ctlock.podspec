Pod::Spec.new do |s|
  s.name             = 'ctlock'
  s.version          = '0.1.0'
  s.summary          = 'POC for AXA erl lock'
  s.swift_version    = '4.2'

  s.description      = <<-DESC
    Proof of concept library to connect with the AXA Erl lock. For demo purposes only
                       DESC

  s.homepage         = 'https://bitbucket.org/nfnty_admin/ctlock_ios'
  s.license          = { :type => 'proprietary', :file => 'LICENSE' }
  s.author           = { 'Conneqtech B.V.' => 'info@conneqtech.com' }
  s.source           = { :git => 'https://bitbucket.org/nfnty_admin/ctlock_ios.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.source_files = 'Source/**/*.swift'
end
