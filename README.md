# Conneqtech Podspecs

To use our private Pod repository you can add it to your cocoapods installation using the following command:

``pod repo add conneqtech https://bitbucket.org/nfnty_admin/conneqtech-specs.git``

After this you can add our source to your own Podfile:

``source 'https://bitbucket.org/nfnty_admin/conneqtech-specs.git'``

This will enable you to add our SDK's as you're used to:

``pod 'ctkit'``


More information can be found at: [Private Cocoapods](https://guides.cocoapods.org/making/private-cocoapods.html)