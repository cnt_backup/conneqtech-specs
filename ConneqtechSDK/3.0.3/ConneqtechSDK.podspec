Pod::Spec.new do |s|
  s.name             = "ConneqtechSDK"
  s.summary          = "iOS SDK that is used in the Conneqtech smart apps"
  s.version          = "3.0.3"
  s.homepage         = "https://bitbucket.org/nfnty_admin/cnt_ios_sdk"
  s.license          = 'MIT'
  s.author           = { "Jens Walrave" => "info@nfnty.nl" }
  s.source           = {
    :git => "git@bitbucket.org:nfnty_admin/cnt_ios_sdk.git",
    :tag => s.version.to_s
  }

  s.ios.deployment_target = '9.0'
  s.swift_version = '4.2'

  s.requires_arc = true
  s.ios.source_files = 'Sources/{iOS,Shared}/**/*'

  s.ios.dependency 'IQKeyboardManagerSwift'
  s.ios.dependency 'AlamofireImage'
end
