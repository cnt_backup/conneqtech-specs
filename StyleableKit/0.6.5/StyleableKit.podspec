Pod::Spec.new do |s|
  s.name             = 'StyleableKit'
  s.version          = '0.6.5'
  s.summary          = 'Styling library for the Conneqtech apps'
  s.swift_version    = '4.2'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://bitbucket.org/nfnty_admin/cnt_ios_styleablekit'
  s.license          = { :type => 'proprietary', :file => 'LICENSE' }
  s.author           = { 'Conneqtech B.V.' => 'info@conneqtech.com' }
  s.source           = { :git => 'git@bitbucket.org:nfnty_admin/cnt_ios_styleablekit.git', :tag => s.version.to_s }

  s.ios.deployment_target = '10.0'
  
  s.source_files = 'StyleableKit/Classes/**/*'

  s.pod_target_xcconfig = { 'SWIFT_VERSION' => '4.2' }
end
